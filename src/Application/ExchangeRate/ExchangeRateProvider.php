<?php

namespace Application\ExchangeRate;

use Money\Currency;

/**
 * Interface ExchangeRateProvider
 */
interface ExchangeRateProvider
{
    public function fetch(Currency $currencyIn, Currency $currencyOut);
}
